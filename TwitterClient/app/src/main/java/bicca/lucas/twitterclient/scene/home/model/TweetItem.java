package bicca.lucas.twitterclient.scene.home.model;

import java.io.Serializable;

public class TweetItem implements Serializable {

    private String profileImageUrl;
    private String profileBannerUrl;
    private String profileTextColor;
    private String profileBackgroundColor;
    private String profileName;
    private String profileUserName;
    private String text;
    private String createdAt;

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    public String getProfileTextColor() {
        return profileTextColor;
    }

    public String getProfileBackgroundColor() {
        return profileBackgroundColor;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getProfileUserName() {
        return profileUserName;
    }

    public String getText() {
        return text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public TweetItem setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
        return this;
    }

    public TweetItem setProfileBannerUrl(String profileBannerUrl) {
        this.profileBannerUrl = profileBannerUrl;
        return this;
    }

    public TweetItem setProfileTextColor(String profileTextColor) {
        this.profileTextColor = profileTextColor;
        return this;
    }

    public TweetItem setProfileBackgroundColor(String profileBackgroundColor) {
        this.profileBackgroundColor = profileBackgroundColor;
        return this;
    }

    public TweetItem setProfileName(String profileName) {
        this.profileName = profileName;
        return this;
    }

    public TweetItem setProfileUserName(String profileUserName) {
        this.profileUserName = profileUserName;
        return this;
    }

    public TweetItem setText(String text) {
        this.text = text;
        return this;
    }

    public TweetItem setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }
}
