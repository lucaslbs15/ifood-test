package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public abstract class UserBase {

    @SerializedName("id")
    protected long id;
    @SerializedName("id_str")
    protected String idStr;
    @SerializedName("name")
    protected String name;
    @SerializedName("screen_name")
    protected String screenName;

    public long getId() {
        return id;
    }

    public String getIdStr() {
        return idStr;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }
}
