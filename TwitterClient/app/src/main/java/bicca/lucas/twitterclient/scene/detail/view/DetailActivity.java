package bicca.lucas.twitterclient.scene.detail.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.Serializable;

import bicca.lucas.twitterclient.R;
import bicca.lucas.twitterclient.databinding.ActivityDetailBinding;
import bicca.lucas.twitterclient.scene.detail.viewmodel.DetailViewModel;
import bicca.lucas.twitterclient.scene.home.model.TweetItem;

public class DetailActivity extends AppCompatActivity {

    public final static String TWEET_ITEM_EXTRAS = "TweetItemExtras";
    private ActivityDetailBinding binding;
    private DetailViewModel detailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        displayAppBar();
        binding.activityDetailProgressBar.setVisibility(View.VISIBLE);
        detailViewModel.setTweetItem((TweetItem) getSerialize(TWEET_ITEM_EXTRAS));
        detailViewModel.prepareApi(getSupportLoaderManager(), this, this);
        detailViewModel.analizeSentiment();
        binding.setDetailViewModel(detailViewModel);
        showTweet();
        initObserverEmoji();
        initObserverError();
    }

    private void displayAppBar() {
        binding.activityDetailToolbar.setNavigationIcon(R.drawable.back_icon);
        setSupportActionBar(binding.activityDetailToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showTweet() {
        TweetItem tweetItem = (TweetItem) getSerialize(TWEET_ITEM_EXTRAS);
        if (tweetItem != null) {
            binding.activityDetailTweet.setTweet(tweetItem);
        }
    }

    private Serializable getSerialize(String extras) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.get(extras) != null) {
            return (Serializable) bundle.get(extras);
        }
        return null;
    }

    private void initObserverEmoji() {
        final Observer<String> observer = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                binding.activityDetailProgressBar.setVisibility(View.GONE);
                binding.activityDetailEmoji.setText(s);
            }
        };
        detailViewModel.getEmojiBySentiment().observe(this, observer);
    }

    private void initObserverError() {
        Observer<Boolean> observer = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    binding.activityDetailProgressBar.setVisibility(View.GONE);
                    Toast.makeText(DetailActivity.this, getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
                }
            }
        };
        detailViewModel.getError().observe(this, observer);
    }

}
