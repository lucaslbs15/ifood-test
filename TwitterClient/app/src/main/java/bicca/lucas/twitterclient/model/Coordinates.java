package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Coordinates {

    @SerializedName("coordinates")
    private float[] coordinates;
    @SerializedName("type")
    private String type;

    public float[] getCoordinates() {
        return coordinates;
    }

    public String getType() {
        return type;
    }
}
