package bicca.lucas.twitterclient.inject.component;

import bicca.lucas.twitterclient.scene.home.viewmodel.MainViewModel;
import bicca.lucas.twitterclient.inject.module.AppModule;
import bicca.lucas.twitterclient.inject.module.RetrofitModule;
import bicca.lucas.twitterclient.inject.module.ServiceModule;
import dagger.Component;

@Component(modules = { AppModule.class, RetrofitModule.class, ServiceModule.class})
public interface AppComponent {

    void inject(MainViewModel mainViewModel);

}
