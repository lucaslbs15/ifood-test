package bicca.lucas.twitterclient.api;

import android.content.Context;

import java.util.List;

import bicca.lucas.twitterclient.model.Tweet;

public class TwitterService extends ServiceBase {

    public TwitterService(Context context, String token, TwitterAPI twitterAPI) {
        super(context, token, twitterAPI);
    }

    public RetrofitLiveData<List<Tweet>> getUserTimeline(int count, String userName) {
        return new RetrofitLiveData<>(twitterAPI.getUserTimeline(token, count, userName));
    }

}
