package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Media {

    @SerializedName("id")
    private long id;
    @SerializedName("display_url")
    private String displayUrl;
    @SerializedName("expanded_url")
    private String expandedUrl;
    @SerializedName("id_str")
    private String idStr;
    @SerializedName("indices")
    private int[] indices;
    @SerializedName("media_url")
    private String mediaUrl;
    @SerializedName("media_url_https")
    private String mediaUrlHttps;
    @SerializedName("sizes")
    private Sizes sizes;
    @SerializedName("source_status_id")
    private long sourceStatusId;
    @SerializedName("source_status_id_str")
    private String sourceStatusIdStr;
    @SerializedName("type")
    private String type;
    @SerializedName("url")
    private String url;

}
