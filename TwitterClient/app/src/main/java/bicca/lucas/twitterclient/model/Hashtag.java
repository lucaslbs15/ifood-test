package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Hashtag {

    @SerializedName("indices")
    private int[] indices;
    @SerializedName("text")
    private String text;
}
