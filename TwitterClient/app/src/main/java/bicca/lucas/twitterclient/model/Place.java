package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Place {

    @SerializedName("id")
    private String id;
    @SerializedName("url")
    private String url;
    @SerializedName("place_type")
    private String placeType;
    @SerializedName("name")
    private String name;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("country")
    private String country;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getPlaceType() {
        return placeType;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountry() {
        return country;
    }
}
