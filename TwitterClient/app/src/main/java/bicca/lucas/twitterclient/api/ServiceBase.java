package bicca.lucas.twitterclient.api;

import android.content.Context;

import retrofit2.Response;

public abstract class ServiceBase {

    protected final Context context;
    protected final String token;
    protected final TwitterAPI twitterAPI;

    public ServiceBase(Context context, String token, TwitterAPI twitterAPI) {
        this.context = context;
        this.token = token;
        this.twitterAPI = twitterAPI;
    }

    protected String handleError(Response response) {
        String error;
        if (response.errorBody() == null) {
            error = response.message();
        } else {
            try {
                error = response.errorBody().string();
            } catch (Exception e) {
                error = response.message();
            }
        }
        return error;
    }
}
