package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Size {

    @SerializedName("w")
    private int w;
    @SerializedName("h")
    private int h;
    @SerializedName("resize")
    private String resize;

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public String getResize() {
        return resize;
    }
}
