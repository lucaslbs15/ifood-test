package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Symbol {

    @SerializedName("indices")
    private int[] indices;
    @SerializedName("text")
    private String text;

    public int[] getIndices() {
        return indices;
    }

    public String getText() {
        return text;
    }
}
