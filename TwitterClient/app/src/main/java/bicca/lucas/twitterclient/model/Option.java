package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Option {

    @SerializedName("position")
    private int position;
    @SerializedName("text")
    private String text;

    public int getPosition() {
        return position;
    }

    public String getText() {
        return text;
    }
}
