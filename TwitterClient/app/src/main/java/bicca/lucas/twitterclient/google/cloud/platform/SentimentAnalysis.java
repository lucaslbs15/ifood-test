package bicca.lucas.twitterclient.google.cloud.platform;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.language.v1.CloudNaturalLanguage;
import com.google.api.services.language.v1.CloudNaturalLanguageRequest;
import com.google.api.services.language.v1.CloudNaturalLanguageScopes;
import com.google.api.services.language.v1.model.AnalyzeSentimentRequest;
import com.google.api.services.language.v1.model.AnalyzeSentimentResponse;
import com.google.api.services.language.v1.model.Document;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import bicca.lucas.twitterclient.google.cloud.platform.model.SentimentInfo;

public class SentimentAnalysis {

    private static final int LOADER_ACCESS_TOKEN = 1;
    private MutableLiveData<SentimentInfo> sentimentInfo;
    private MutableLiveData<Boolean> error;

    private GoogleCredential mCredential;
    private CloudNaturalLanguage mApi = new CloudNaturalLanguage.Builder(
            new NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {
                    mCredential.initialize(request);
                }
            }
    ).build();

    private final BlockingQueue<CloudNaturalLanguageRequest<? extends GenericJson>> mRequests
            = new ArrayBlockingQueue<>(3);

    private Thread mThread;

    public void prepareApi(LoaderManager loaderManager, final Context context) {
        loaderManager.initLoader(LOADER_ACCESS_TOKEN, null,
                new LoaderManager.LoaderCallbacks<String>() {
                    @NonNull
                    @Override
                    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
                        return new AccessTokenLoader(context);
                    }

                    @Override
                    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
                        setAccessToken(data);
                    }

                    @Override
                    public void onLoaderReset(@NonNull Loader<String> loader) {

                    }
                });
    }

    private void setAccessToken(String token) {
        mCredential = new GoogleCredential()
                .setAccessToken(token)
                .createScoped(CloudNaturalLanguageScopes.all());
        startWorkerThread();
    }

    public void analyzeSentiment(String text) {
        try {
            mRequests.add(mApi.documents().analyzeSentiment(new AnalyzeSentimentRequest()
            .setDocument(new Document().setContent(text).setType("PLAIN_TEXT"))));
        } catch (IOException e) {
        }
    }

    private void startWorkerThread() {
        if (mThread != null) {
            return;
        }
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (mThread == null) {
                        break;
                    }

                    try {
                        deliverResponse(mRequests.take().execute());
                    } catch (InterruptedException e) {
                        error.postValue(true);
                        break;
                    } catch (IOException e) {
                        error.postValue(true);
                    }
                }
            }
        });
        mThread.start();
    }

    private void deliverResponse(GenericJson response) {
        if (response instanceof AnalyzeSentimentResponse) {
            sentimentInfo.postValue(new SentimentInfo(
                    ((AnalyzeSentimentResponse) response).getDocumentSentiment()));
        }
    }

    public MutableLiveData<SentimentInfo> getSentimentInfo() {
        if (sentimentInfo == null) {
            sentimentInfo = new MutableLiveData<>();
        }
        return sentimentInfo;
    }

    public MutableLiveData<Boolean> getError() {
        if (error == null) {
            error = new MutableLiveData<>();
        }
        return error;
    }

}
