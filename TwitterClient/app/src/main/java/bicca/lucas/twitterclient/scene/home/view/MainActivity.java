package bicca.lucas.twitterclient.scene.home.view;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bicca.lucas.twitterclient.R;
import bicca.lucas.twitterclient.TwitterClientApplication;
import bicca.lucas.twitterclient.databinding.ActivityMainBinding;
import bicca.lucas.twitterclient.scene.detail.view.DetailActivity;
import bicca.lucas.twitterclient.scene.home.model.TweetItem;
import bicca.lucas.twitterclient.scene.home.viewmodel.MainViewModel;
import bicca.lucas.twitterclient.model.Tweet;

public class MainActivity extends AppCompatActivity implements TweetAdapter.OnItemSelected {

    private ActivityMainBinding binding;
    MainViewModel mainViewModel;
    private TextView.OnEditorActionListener searchListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchUser();
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding.activityMainSearch.clearFocus();
        displayAppBar();
        initInject();
        initObserverError();
        initObserverTweets();
        binding.activityMainSearch.setOnEditorActionListener(searchListener);
    }

    private void displayAppBar() {
        binding.activityMainToolbar.setNavigationIcon(R.drawable.ic_twitter);
        setSupportActionBar(binding.activityMainToolbar);
    }

    private void initObserverTweets() {
        final Observer<List<Tweet>> tweetsObserver = new Observer<List<Tweet>>() {
            @Override
            public void onChanged(@Nullable List<Tweet> tweets) {
                showTweets(tweets);
            }
        };
        mainViewModel.getTweetList().observe(this, tweetsObserver);
    }

    private void initObserverError() {
        Observer<Boolean> errorObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean error) {
                if (error) {
                    binding.activityMainProgressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
                }
            }
        };
        mainViewModel.getError().observe(this, errorObserver);
    }

    private void initInject() {
        ((TwitterClientApplication) getApplication()).getAppComponent().inject(mainViewModel);
    }

    private void searchUser() {
        hideKeyBoard();
        binding.activityMainProgressBar.setVisibility(View.VISIBLE);
        mainViewModel.initLiveData(100, binding.activityMainSearch.getText().toString());
    }

    private void showTweets(List<Tweet> tweets) {
        binding.activityMainRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.activityMainRecyclerView.setLayoutManager(linearLayoutManager);
        TweetAdapter adapter = new TweetAdapter(tweets, this);
        binding.activityMainRecyclerView.setAdapter(adapter);
        binding.activityMainProgressBar.setVisibility(View.GONE);
    }

    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public void onItemClick(TweetItem tweet) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(DetailActivity.TWEET_ITEM_EXTRAS, tweet);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
