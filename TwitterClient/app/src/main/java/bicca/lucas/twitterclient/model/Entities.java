package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Entities {

    @SerializedName("hashtags")
    private Hashtag[] hashtags;
    @SerializedName("urls")
    private URL[] urls;
    @SerializedName("user_mentions")
    private UserMention[] userMentions;
    @SerializedName("symbols")
    private Symbol[] symbols;

    public Hashtag[] getHashtags() {
        return hashtags;
    }

    public URL[] getUrls() {
        return urls;
    }

    public UserMention[] getUserMentions() {
        return userMentions;
    }

    public Symbol[] getSymbols() {
        return symbols;
    }
}
