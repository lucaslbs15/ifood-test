package bicca.lucas.twitterclient;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class BindingAdapterUtils {

    private static int converColor(String colorStr) {
        StringBuilder stringBuilder = new StringBuilder(colorStr);
        stringBuilder.insert(0, "#");
        return Color.parseColor(stringBuilder.toString());
    }

    @BindingAdapter({"imageUrl", "error"})
    public static void loadImage(ImageView view, String url, Drawable error) {
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .cornerRadiusDp(30)
                .oval(false)
                .build();
        Picasso.with(view.getContext()).load(url).transform(transformation).error(error).into(view);
    }

    @BindingAdapter("backgroundStr")
    public static void setBackgroundByString(View view, String background) {
        view.setBackgroundColor(converColor(background));
    }

    @BindingAdapter("textColorStr")
    public static void setTextColorByString(TextView textView, String color) {
        textView.setTextColor(converColor(color));
    }
}
