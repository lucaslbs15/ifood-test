package bicca.lucas.twitterclient.inject.module;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import bicca.lucas.twitterclient.api.TwitterAPI;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {

    private String baseUrl;

    public RetrofitModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    public Cache provideCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    public OkHttpClient provideOkttpClient(Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .cache(cache)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        return builder.build();
    }

    @Provides
    public TwitterAPI provideTwitterAPI(OkHttpClient okHttpClient) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(baseUrl);
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.client(okHttpClient);
        return builder.build().create(TwitterAPI.class);
    }
}
