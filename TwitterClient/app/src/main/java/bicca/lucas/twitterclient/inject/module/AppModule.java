package bicca.lucas.twitterclient.inject.module;

import android.app.Application;
import android.content.Context;

import bicca.lucas.twitterclient.TwitterClientApplication;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final TwitterClientApplication mainApp;

    public AppModule(TwitterClientApplication mainApp) {
        this.mainApp = mainApp;
    }

    @Provides
    public Application provideApplication() {
        return mainApp;
    }

    @Provides
    public Context provideContext() {
        return mainApp;
    }
}
