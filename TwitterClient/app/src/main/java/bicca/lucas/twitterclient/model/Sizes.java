package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Sizes {

    @SerializedName("thumb")
    private Size thumb;
    @SerializedName("large")
    private Size large;
    @SerializedName("medium")
    private Size medium;
    @SerializedName("small")
    private Size small;

    public Size getThumb() {
        return thumb;
    }

    public Size getLarge() {
        return large;
    }

    public Size getMedium() {
        return medium;
    }

    public Size getSmall() {
        return small;
    }
}
