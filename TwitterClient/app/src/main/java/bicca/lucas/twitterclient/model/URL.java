package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class URL {

    @SerializedName("display_url")
    private String displayUrl;
    @SerializedName("expanded_url")
    private String expandedUrl;
    @SerializedName("indices")
    private int[] indices;
    @SerializedName("url")
    private String url;
}
