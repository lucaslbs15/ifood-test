package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Poll {

    @SerializedName("options")
    private Option[] options;
    @SerializedName("end_datetime")
    private String endDatetime;
    @SerializedName("duration_minutes")
    private String durationMinutes;

}
