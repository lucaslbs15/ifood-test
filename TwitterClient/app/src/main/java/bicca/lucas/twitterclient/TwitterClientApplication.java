package bicca.lucas.twitterclient;

import android.app.Application;

import bicca.lucas.twitterclient.inject.component.AppComponent;
import bicca.lucas.twitterclient.inject.component.DaggerAppComponent;
import bicca.lucas.twitterclient.inject.module.AppModule;
import bicca.lucas.twitterclient.inject.module.RetrofitModule;
import bicca.lucas.twitterclient.inject.module.ServiceModule;

public class TwitterClientApplication extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initInject();
    }

    private void initInject() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .retrofitModule(new RetrofitModule(getString(R.string.twitter_api)))
                    .serviceModule(new ServiceModule())
                    .build();
        }
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
