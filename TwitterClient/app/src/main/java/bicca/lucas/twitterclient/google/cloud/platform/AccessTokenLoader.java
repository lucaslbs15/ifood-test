package bicca.lucas.twitterclient.google.cloud.platform;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.language.v1.CloudNaturalLanguageScopes;

import java.io.IOException;
import java.io.InputStream;

import bicca.lucas.twitterclient.R;

public class AccessTokenLoader extends AsyncTaskLoader<String> {

    private static final String PREFS = "AccessTokenLoader";
    private static final String PREF_ACCESS_TOKEN = "access_token";

    public AccessTokenLoader(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public String loadInBackground() {
        SharedPreferences sharedPreferences =
                getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(PREF_ACCESS_TOKEN, null);
        if (token != null) {
            GoogleCredential googleCredential =
                    new GoogleCredential()
                    .setAccessToken(token)
                    .createScoped(CloudNaturalLanguageScopes.all());
            Long seconds = googleCredential.getExpiresInSeconds();
            if (seconds != null && seconds > 3600) {
                return token;
            }
        }

        InputStream inputStream = getContext().getResources().openRawResource(R.raw.credential);
        try {
            GoogleCredential googleCredential =
                    GoogleCredential.fromStream(inputStream)
                    .createScoped(CloudNaturalLanguageScopes.all());
            googleCredential.refreshToken();
            String newtoken = googleCredential.getAccessToken();
            sharedPreferences.edit().putString(PREF_ACCESS_TOKEN, newtoken).apply();
            return newtoken;
        } catch (IOException e) {
        }
        return null;
    }
}
