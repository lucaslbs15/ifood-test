package bicca.lucas.twitterclient.scene.detail.viewmodel;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;

import bicca.lucas.twitterclient.google.cloud.platform.SentimentAnalysis;
import bicca.lucas.twitterclient.google.cloud.platform.model.SentimentInfo;
import bicca.lucas.twitterclient.scene.home.model.TweetItem;

public class DetailViewModel extends ViewModel {

    private SentimentAnalysis sentimentAnalysis;
    private TweetItem tweetItem;
    private MutableLiveData<String> emojiBySentiment;
    private MutableLiveData<Boolean> error;

    public void setTweetItem(TweetItem tweetItem) {
        this.tweetItem = tweetItem;
    }

    public void prepareApi(LoaderManager loaderManager, Context context, LifecycleOwner lifecycleOwner) {
        sentimentAnalysis = new SentimentAnalysis();
        sentimentAnalysis.prepareApi(loaderManager, context);
        initObserverSentimentInfo(lifecycleOwner);
        initObserverSentimentInfoError(lifecycleOwner);
    }

    public void analizeSentiment() {
        sentimentAnalysis.analyzeSentiment(tweetItem.getText());
    }

    public MutableLiveData<String> getEmojiBySentiment() {
        if (emojiBySentiment == null) {
            emojiBySentiment = new MutableLiveData<>();
        }
        return emojiBySentiment;
    }

    private int getSentimentUnicode(SentimentInfo sentimentInfo) {
        int unicode;
        switch (sentimentInfo.getSentimentLevel()) {
            case HAPPY:
                unicode = 0x1F603;
                break;
            case NEUTRAL:
                unicode = 0x1F610;
                break;
            case SAD:
                unicode = 0x1F614;
                break;
            default:
                unicode = 0x1F636;
        }
        return unicode;
    }

    public TweetItem getTweetItem() {
        return tweetItem;
    }

    private void initObserverSentimentInfo(LifecycleOwner lifecycleOwner) {
        Observer<SentimentInfo> observer = new Observer<SentimentInfo>() {
            @Override
            public void onChanged(@Nullable SentimentInfo sentimentInfo) {
                int unicode = getSentimentUnicode(sentimentInfo);
                emojiBySentiment.setValue(new String(Character.toChars(unicode)));
            }
        };
        sentimentAnalysis.getSentimentInfo().observe(lifecycleOwner, observer);
    }

    public MutableLiveData<Boolean> getError() {
        if (error == null) {
            error = new MutableLiveData<>();
        }
        return error;
    }

    private void initObserverSentimentInfoError(LifecycleOwner lifecycleOwner) {
        Observer<Boolean> observer = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                error.setValue(true);
                emojiBySentiment.setValue(new String(Character.toChars(0x1F636)));
            }
        };
        sentimentAnalysis.getError().observe(lifecycleOwner, observer);
    }
}
