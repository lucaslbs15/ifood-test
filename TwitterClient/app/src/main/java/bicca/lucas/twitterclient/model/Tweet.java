package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class Tweet {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("id")
    private long id;
    @SerializedName("id_str")
    private String idStr;
    @SerializedName("text")
    private String text;
    @SerializedName("source")
    private String source;
    @SerializedName("truncated")
    private boolean truncated;
    @SerializedName("in_reply_to_status_id")
    private long inReplyToStatusId;
    @SerializedName("in_reply_to_status_id_str")
    private String inReplyToStatusIdStr;
    @SerializedName("in_reply_to_user_id")
    private long inReplayToUserId;
    @SerializedName("in_reply_to_user_id_str")
    private String inReplyToUserIdStr;
    @SerializedName("in_reply_to_screen_name")
    private String inReplyToScreenName;
    @SerializedName("user")
    private User user;
    @SerializedName("coordinates")
    private Coordinates coordinates;
    @SerializedName("place")
    private Place place;
    @SerializedName("quoted_status_id")
    private long quotedStatusId;
    @SerializedName("quoted_status_id_str")
    private String quotedStatusIdStr;
    @SerializedName("is_quote_status")
    private boolean isQuoteStatus;
    @SerializedName("quoted_status")
    private Tweet tweet;
    @SerializedName("retweeted_status")
    private Tweet retweetedStatus;
    @SerializedName("quote_count")
    private int quoteCount;
    @SerializedName("reply_count")
    private int replyCount;
    @SerializedName("retweet_count")
    private int retweetCount;
    @SerializedName("favorite_count")
    private int favouriteCount;
    @SerializedName("entities")
    private Entities entities;
    @SerializedName("favorited")
    private boolean favorited;
    @SerializedName("retweeted")
    private boolean retweeted;
    @SerializedName("possibly_sensitive")
    private boolean possiblySensitive;
    @SerializedName("filter_level")
    private String filterLevel;
    @SerializedName("lang")
    private String language;

    public String getCreatedAt() {
        return createdAt;
    }

    public long getId() {
        return id;
    }

    public String getIdStr() {
        return idStr;
    }

    public String getText() {
        return text;
    }

    public String getSource() {
        return source;
    }

    public boolean isTruncated() {
        return truncated;
    }

    public long getInReplyToStatusId() {
        return inReplyToStatusId;
    }

    public String getInReplyToStatusIdStr() {
        return inReplyToStatusIdStr;
    }

    public long getInReplayToUserId() {
        return inReplayToUserId;
    }

    public String getInReplyToUserIdStr() {
        return inReplyToUserIdStr;
    }

    public String getInReplyToScreenName() {
        return inReplyToScreenName;
    }

    public User getUser() {
        return user;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Place getPlace() {
        return place;
    }

    public long getQuotedStatusId() {
        return quotedStatusId;
    }

    public String getQuotedStatusIdStr() {
        return quotedStatusIdStr;
    }

    public boolean isQuoteStatus() {
        return isQuoteStatus;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public Tweet getRetweetedStatus() {
        return retweetedStatus;
    }

    public int getQuoteCount() {
        return quoteCount;
    }

    public int getReplyCount() {
        return replyCount;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public int getFavouriteCount() {
        return favouriteCount;
    }

    public Entities getEntities() {
        return entities;
    }

    public boolean isFavorited() {
        return favorited;
    }

    public boolean isRetweeted() {
        return retweeted;
    }

    public boolean isPossiblySensitive() {
        return possiblySensitive;
    }

    public String getFilterLevel() {
        return filterLevel;
    }

    public String getLanguage() {
        return language;
    }
}
