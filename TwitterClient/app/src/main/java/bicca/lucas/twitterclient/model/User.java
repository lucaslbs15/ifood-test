package bicca.lucas.twitterclient.model;

import com.google.gson.annotations.SerializedName;

public class User extends UserBase {

    @SerializedName("location")
    private String location;
    @SerializedName("url")
    private String url;
    @SerializedName("description")
    private String description;
    @SerializedName("protected")
    private boolean isProtected;
    @SerializedName("followers_count")
    private int followersCount;
    @SerializedName("friends_count")
    private int friendsCount;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("favourites_count")
    private int favouritesCount;
    @SerializedName("utc_offset")
    private String utcOffset;
    @SerializedName("time_zone")
    private String timeZone;
    @SerializedName("geo_enabled")
    private boolean geoEnabled;
    @SerializedName("verified")
    private boolean verified;
    @SerializedName("statuses_count")
    private int statusesCount;
    @SerializedName("lang")
    private String language;
    @SerializedName("contributors_enabled")
    private boolean contributorsEnabled;
    @SerializedName("is_translator")
    private boolean isTranslator;
    @SerializedName("is_translation_enabled")
    private boolean isTranslationEnabled;
    @SerializedName("profile_background_color")
    private String profileBackgroundColor;
    @SerializedName("profile_background_image_url")
    private String profileBackgroundImageUrl;
    @SerializedName("profile_background_image_url_https")
    private String getProfileBackgroundImageUrlHttps;
    @SerializedName("profile_background_tile")
    private boolean profileBackgroundTile;
    @SerializedName("profile_image_url")
    private String profileImageUrl;
    @SerializedName("profile_image_url_https")
    private String profileImageUrlHttps;
    @SerializedName("profile_banner_url")
    private String profileBannerUrl;
    @SerializedName("profile_link_color")
    private String profileLinkColor;
    @SerializedName("profile_sidebar_border_color")
    private String profileSidebarBorderColor;
    @SerializedName("profile_sidebar_fill_color")
    private String profileSidebarFillColor;
    @SerializedName("profile_text_color")
    private String profileTextColor;
    @SerializedName("profile_use_background_image")
    private boolean profileUseBackgroundImage;
    @SerializedName("has_extended_profile")
    private boolean hasExtenderProfile;
    @SerializedName("default_profile")
    private boolean defaultProfile;
    @SerializedName("default_profile_image")
    private boolean defaultProfileImage;
    @SerializedName("following")
    private boolean following;
    @SerializedName("follow_request_sent")
    private boolean followRequestSent;
    @SerializedName("notifications")
    private boolean notifications;
    @SerializedName("muting")
    private boolean muting;
    @SerializedName("blocking")
    private boolean blocking;
    @SerializedName("translator_type")
    private String translatorType;

    public String getLocation() {
        return location;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getFavouritesCount() {
        return favouritesCount;
    }

    public String getUtcOffset() {
        return utcOffset;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public boolean isGeoEnabled() {
        return geoEnabled;
    }

    public boolean isVerified() {
        return verified;
    }

    public int getStatusesCount() {
        return statusesCount;
    }

    public String getLanguage() {
        return language;
    }

    public boolean isContributorsEnabled() {
        return contributorsEnabled;
    }

    public boolean isTranslator() {
        return isTranslator;
    }

    public boolean isTranslationEnabled() {
        return isTranslationEnabled;
    }

    public String getProfileBackgroundColor() {
        return profileBackgroundColor;
    }

    public String getProfileBackgroundImageUrl() {
        return profileBackgroundImageUrl;
    }

    public String getGetProfileBackgroundImageUrlHttps() {
        return getProfileBackgroundImageUrlHttps;
    }

    public boolean isProfileBackgroundTile() {
        return profileBackgroundTile;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    public String getProfileLinkColor() {
        return profileLinkColor;
    }

    public String getProfileSidebarBorderColor() {
        return profileSidebarBorderColor;
    }

    public String getProfileSidebarFillColor() {
        return profileSidebarFillColor;
    }

    public String getProfileTextColor() {
        return profileTextColor;
    }

    public boolean isProfileUseBackgroundImage() {
        return profileUseBackgroundImage;
    }

    public boolean isHasExtenderProfile() {
        return hasExtenderProfile;
    }

    public boolean isDefaultProfile() {
        return defaultProfile;
    }

    public boolean isDefaultProfileImage() {
        return defaultProfileImage;
    }

    public boolean isFollowing() {
        return following;
    }

    public boolean isFollowRequestSent() {
        return followRequestSent;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public boolean isMuting() {
        return muting;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public String getTranslatorType() {
        return translatorType;
    }
}
