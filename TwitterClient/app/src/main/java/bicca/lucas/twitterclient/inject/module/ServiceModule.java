package bicca.lucas.twitterclient.inject.module;

import android.content.Context;

import bicca.lucas.twitterclient.R;
import bicca.lucas.twitterclient.api.TwitterAPI;
import bicca.lucas.twitterclient.api.TwitterService;
import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    public TwitterService provideTwitterService(Context context, TwitterAPI twitterAPI) {
        String token = context.getString(R.string.twitter_key);
        return new TwitterService(context, token, twitterAPI);
    }
}
