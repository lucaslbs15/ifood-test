package bicca.lucas.twitterclient.scene.home.view;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bicca.lucas.twitterclient.BR;
import bicca.lucas.twitterclient.R;
import bicca.lucas.twitterclient.scene.home.model.TweetItem;
import bicca.lucas.twitterclient.model.Tweet;

public class TweetAdapter extends RecyclerView.Adapter<TweetAdapter.ItemTweetViewHolder> {

    private List<Tweet> items;
    private OnItemSelected listener;

    public TweetAdapter(List<Tweet> items, OnItemSelected listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemTweetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);
        return new ItemTweetViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemTweetViewHolder holder, int position) {
        TweetItem tweet = createTweetItem(items.get(position));
        holder.bind(tweet, listener);
    }

    private TweetItem createTweetItem(Tweet tweet) {
        return new TweetItem()
                .setProfileImageUrl(tweet.getUser().getProfileImageUrl())
                .setProfileBannerUrl(tweet.getUser().getProfileBannerUrl())
                .setProfileTextColor(tweet.getUser().getProfileTextColor())
                .setProfileBackgroundColor(tweet.getUser().getProfileBackgroundColor())
                .setProfileName(tweet.getUser().getName())
                .setProfileUserName(tweet.getUser().getScreenName())
                .setText(tweet.getText())
                .setCreatedAt(tweet.getCreatedAt());
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.item_tweet;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemTweetViewHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        public ItemTweetViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Object object, final OnItemSelected listener) {
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick((TweetItem) object);
                }
            });
            binding.setVariable(BR.tweet, object);
            binding.executePendingBindings();
        }
    }

    public interface OnItemSelected {
        void onItemClick(TweetItem tweet);
    }
}
