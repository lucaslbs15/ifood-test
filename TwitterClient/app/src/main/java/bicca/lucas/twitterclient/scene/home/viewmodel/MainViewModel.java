package bicca.lucas.twitterclient.scene.home.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import javax.inject.Inject;

import bicca.lucas.twitterclient.api.RetrofitLiveData;
import bicca.lucas.twitterclient.api.TwitterService;
import bicca.lucas.twitterclient.google.cloud.platform.SentimentAnalysis;
import bicca.lucas.twitterclient.model.Tweet;

public class MainViewModel extends ViewModel {

    @Inject
    TwitterService twitterService;
    private RetrofitLiveData<List<Tweet>> liveData;
    private MutableLiveData<List<Tweet>> tweetList;
    private MutableLiveData<Boolean> error;

    public void initLiveData(int count, String userName) {
        liveData = twitterService.getUserTimeline(count, userName);
        initObservable();
    }

    private void initObservable() {
        liveData.observeForever(new Observer<List<Tweet>>() {
            @Override
            public void onChanged(@Nullable List<Tweet> tweets) {
                if (CollectionUtils.isNotEmpty(tweets)) {
                    tweetList.setValue(tweets);
                } else {
                    error.setValue(true);
                }
            }
        });
    }

    public MutableLiveData<List<Tweet>> getTweetList() {
        if (tweetList == null) {
            tweetList = new MutableLiveData<>();
        }
        return tweetList;
    }

    public MutableLiveData<Boolean> getError() {
        if (error == null) {
            error = new MutableLiveData<>();
        }
        return error;
    }
}
