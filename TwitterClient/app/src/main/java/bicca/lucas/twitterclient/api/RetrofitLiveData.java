package bicca.lucas.twitterclient.api;

import android.arch.lifecycle.LiveData;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.bag.CollectionSortedBag;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitLiveData<T> extends LiveData<T> implements Callback<T> {

    private Call<T> call;

    public RetrofitLiveData(Call<T> call) {
        this.call = call;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        setValue(response.body());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        setValue(null);
    }

    public void cancel() {
        if (!call.isCanceled()) {
            call.cancel();
        }
    }

    @Override
    protected void onActive() {
        if (!call.isCanceled() && !call.isExecuted()) {
            call.enqueue(this);
        }
    }
}
