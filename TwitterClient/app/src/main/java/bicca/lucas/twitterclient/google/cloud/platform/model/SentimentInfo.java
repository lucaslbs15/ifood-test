package bicca.lucas.twitterclient.google.cloud.platform.model;

import com.google.api.services.language.v1.model.Sentiment;

public class SentimentInfo {

    private float score;
    private float magnitude;

    public enum SentimentLevel {
        HAPPY, NEUTRAL, SAD
    }

    public SentimentInfo(Sentiment sentiment) {
        this.score = sentiment.getScore();
        this.magnitude = sentiment.getMagnitude();
    }

    public float getScore() {
        return score;
    }

    public float getMagnitude() {
        return magnitude;
    }

    public SentimentLevel getSentimentLevel() {
        SentimentLevel sentimentLevel;
        if (score > 0.25) {
            sentimentLevel = SentimentLevel.HAPPY;
        } else if (score > -0.75) {
            sentimentLevel = SentimentLevel.NEUTRAL;
        } else {
            sentimentLevel = SentimentLevel.SAD;
        }
        return sentimentLevel;
    }
}
