package bicca.lucas.twitterclient.api;

import java.util.List;

import bicca.lucas.twitterclient.model.Tweet;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface TwitterAPI {

    @Headers("Content-Type: application/json")
    @GET("/1.1/statuses/user_timeline.json")
    Call<List<Tweet>> getUserTimeline(
            @Header("Authorization") String token,
            @Query("count") int count,
            @Query("screen_name") String userName);
}
